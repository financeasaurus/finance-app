import React, { Component} from "react";
import {hot} from "react-hot-loader";
import { Switch, Route, Link } from 'react-router-dom';
import "./App.css";
window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;

import Header from "./components/Header";
import Home from "./components/Home";
import AccountsRouter from "./components/accounts/AccountsRouter";
import Login from "./components/login/Login";
import TransactionsRouter from "./components/transactions/TransactionsRouter";
import ReportsRouter from "./components/spending_reports/ReportsRouter";

class App extends Component {	
	constructor() {
		super();
		this.state = {
			isAuthenticated: false
		}
		this.handleAuthSucceed = this.handleAuthSucceed.bind(this);
	}

	// TODO: Figure out how to handle a localstorage wipe
	async componentDidMount() {		
		// We need to check for the token on a page refresh
		if (localStorage.getItem("status") !== null) {
			await this.setState({isAuthenticated: true})
		}
	}
	
	async handleAuthSucceed() {
		// TODO: Error checking - unless handled by login page
		// We set a status flag to keep track that the user is authenticated
		localStorage.setItem("status", true);
		await this.setState({isAuthenticated: true});
	}

	async handleLogoutSucceed() {
		// TODO: Error checking - unless handled by login page
		// We remove a status flag to say the user is no longer authenticated
		localStorage.removeItem("status");
		await this.setState({isAuthenticated: false});
	}
	
	render(){
		//<Route path='/accounts' render={(props) => <AccountsRouter {...props} user_id={this.state.user_id} isAuthenticated={this.state.isAuthenticated} />} />
		return(
			<div className="main-grid">
				<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"></link>
				<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"></link>
				<div className="main-header"><Header isAuthenticated={this.state.isAuthenticated}/></div>
				<div className="main-body">
				<Switch>
					<Route exact path='/' component={Home}/>
					<Route exact path='/home' component={Home} />
					<Route path="/login" render={(props) => <Login {...props} onAuthSucceed={this.handleAuthSucceed} isAuthenticated={this.state.isAuthenticated}  />} />
					<Route path="/logout" render={() => (<div> Logout not implemented yet </div>)} />
					<Route path='/accounts' render={(props) => <AccountsRouter {...this.state} handleLogoutSucceed={this.handleLogoutSucceed} />} />
					<Route path='/transactions' render={(props) => <TransactionsRouter {...props} user_id={this.state.user_id} isAuthenticated={this.state.isAuthenticated} />} />
					<Route path='/spending_reports' render={(props) => <ReportsRouter {...props} user_id={this.state.user_id} isAuthenticated={this.state.isAuthenticated} />} />
					<Route render={() => (<div> Sorry, this page does not exist. </div>)} />
				</Switch></div>
			</div>
		);
	}
}
export default hot(module)(App);
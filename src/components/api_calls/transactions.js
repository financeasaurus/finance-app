import axios from 'axios';
axios.defaults.withCredentials = true
// TODO: Variablilze the api server url
// TODO: Actually do error handling

const url = "http://localhost:8080";

const get_transactions = async (year) => {
    return axios.get(`${url}/api/transactions?year=${year}`)
    .then(response => response.data)
    .then(data => {
        console.log(data)
        return data
    })
    .catch(error => console.log(error));
}

const get_transactions_years = async () => {
    return axios.get(`${url}/api/transactions/years`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

const post_transactions = async (transaction) => {
    return axios.post(`${url}/api/transactions`, {
        amount: transaction.amount,
        account_id: transaction.account_id,
        transaction_type_id: transaction.transaction_type_id,
        date: transaction.date,
        description: transaction.description
    })
    .then(response => response.data)
    .then(data => {
        return data["transaction_id"];
    })
    .catch(error => console.log(error));
}

const put_transaction = async (transaction) => {
    return axios.put(`${url}/api/transaction/${transaction.transaction_id}`, {
        amount: transaction.amount,
        account_id: transaction.account_id,
        transaction_type_id: transaction.transaction_type_id,
        date: transaction.date,
        description: transaction.description
    })
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

const delete_transaction = async (transaction_id) => {
    return axios.delete(`${url}/api/transaction/${transaction_id}`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

module.exports = {
    get_transactions, get_transactions_years, post_transactions, put_transaction, delete_transaction
}
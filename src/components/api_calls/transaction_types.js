import axios from 'axios';
axios.defaults.withCredentials = true
// TODO: Variablilze the api server url
// TODO: Actually do error handling

const url = "http://localhost:8080";

const get_transaction_types = async () => {
    return axios.get(`${url}/api/transaction_types`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

module.exports = {
    get_transaction_types
}
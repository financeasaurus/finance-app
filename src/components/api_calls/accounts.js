import axios from 'axios';
axios.defaults.withCredentials = true
// TODO: Variablilze the api server url
// TODO: Actually do error handling

const url = "http://localhost:8080";

const get_accounts = async () => {
    return axios.get(`${url}/api/accounts`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

const get_account = async (account_id) => {
    return axios.get(`${url}/api/account/${account_id}`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

const get_account_balances = async (account_id) => {
    return axios.get(`${url}/api/account/${account_id}/balances`)
    .then(response => response.data)
    .then(data => {
        return data[0];
    })
    .catch(error => console.log(error));
}

const create_account = async (name, description) => {
    return axios.post(`${url}/api/accounts`, {
        name: name,
        description: description
    })
    .then(response => response.data)
    .then((data) => {
        return data["account_id"];
    })
    .catch((error) => {
        console.log(error);
    })
}

const modify_account = async (name, description) => {
    return axios.put(`${url}/api/account/${account_id}`, {
        name: name,
        description: description
    })
    .then(response => response.data)
    .then((data) => {
        return data["account_id"];
    })
    .catch((error) => {
        console.log(error);
    })
}

module.exports = {
    get_accounts, get_account, get_account_balances, create_account, modify_account
}
import axios from 'axios';
// TODO: Variablilze the api server url
// TODO: Actually do error handling
axios.defaults.withCredentials = true

const url = "http://localhost:8080";

const user_login = async (username, password) => {
    //let token = btoa(`${username}:${password}`);
    return await axios.post(`${url}/login`, {},
    {
        auth: {
            username: username,
            password: password
        }
    })
    .then(response => {
        return response.data
    })
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

module.exports = {
    user_login
}
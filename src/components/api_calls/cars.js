import axios from 'axios';
axios.defaults.withCredentials = true
// TODO: Variablilze the api server url
// TODO: Actually do error handling

const url = "http://localhost:8080";

const get_cars = async () => {
    return axios.get(`${url}/api/cars`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}
module.exports = {
    get_cars
}
import axios from 'axios';
axios.defaults.withCredentials = true
// TODO: Variablilze the api server url
// TODO: Actually do error handling

const url = "http://localhost:8080";

const account_balances = async (account_id) => {
    return axios.get(`${url}/api/account/${account_id}/balances`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}
module.exports = {
    account_balances
}
import axios from 'axios';
axios.defaults.withCredentials = true
// TODO: Variablilze the api server url
// TODO: Actually do error handling

const url = "http://localhost:8080";

const get_gas_transactions = async (year) => {
    return axios.get(`${url}/api/gas_transactions?year=${year}`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

const get_gas_years = async () => {
    return axios.get(`${url}/api/gas_transactions/years`)
    .then(response => response.data)
    .then(data => {
        return data;
    })
    .catch(error => console.log(error));
}

module.exports = {
    get_gas_transactions, get_gas_years
}
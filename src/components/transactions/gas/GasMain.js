import React, { Component} from "react";
import {hot} from "react-hot-loader";
import moment from "moment";
// Material UI
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
// Finance App
import {get_cars} from "../../api_calls/cars";
import CarsTable from "../../cars/CarsTable";
import {get_gas_transactions, get_gas_years} from "../../api_calls/gas_transactions";
import GasTable from "./GasTable";
import GasAdd from "./GasAdd";
import {get_accounts} from "../../api_calls/accounts";

class GasMain extends Component {
	constructor(props) {
		super(props);
		this.state = {
			cars: [],
			selectedYear: (new Date()).getFullYear(),
			gasTransactions: [],
			gasYears: [],
			accounts: []
		}
		this.handleYearSelection = this.handleYearSelection.bind(this);
		this.postGasTransaction = this.postGasTransaction.bind(this);
	}
	
	async componentDidMount() {
		// TODO: Make these calls parallel
		let cars = await get_cars();
		let gasTransactions = await get_gas_transactions(this.state.selectedYear);
		let gasYears = await get_gas_years();
		let accounts = await get_accounts();
		
		let yearFound = gasYears.find(year => year === this.state.selectedYear);
		if (yearFound === undefined) {gasYears.unshift(this.state.selectedYear)};

		this.setState({cars: cars, gasTransactions: gasTransactions, gasYears: gasYears, accounts: accounts});
	}

	// This will handle the year being updated
	async handleYearSelection(year) {
		// We will need to load transactions for the selected year's data
		if (year != this.state.selectedYear) {
			let gasTransactions = await get_gas_transactions(year);
			this.setState({selectedYear: year, gasTransactions: gasTransactions});
		}
	}

	// TODO: Add API calls
	async postGasTransaction(transaction) {
		console.log("postGas", transaction);
		console.log("total", (transaction.price * transaction.gallons));
	}
	
	// TODO: Allow for editing and deletion inside of the table
	render(){
		return(
			<div className="gas-grid">
				<CarsTable cars={this.state.cars}/>
				<InputLabel htmlFor="year-select">Select Year</InputLabel>
				<div className={"gas-year-select"} style={{ width: "20%" }}>
					<Select onChange={e => this.handleYearSelection(e.target.value)} value={this.state.selectedYear}>
						{this.state.gasYears.map((year, index) => <MenuItem key={index} value={year}>{year}</MenuItem>)}
					</Select>
				</div>
				<div className={"gas-new-button"}>
					<GasAdd cars={this.state.cars} accounts={this.state.accounts} postGasTransaction={this.postGasTransaction}/>
				</div>
				<div className={"gas-data-table"}>
					<GasTable gasTransactions={this.state.gasTransactions} cars={this.state.cars} accounts={this.state.accounts}/>
				</div>
			</div>
		);
	}
}

export default hot(module)(GasMain);
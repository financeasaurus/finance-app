import React, { Component} from "react";
import {hot} from "react-hot-loader";
import { Switch, Route, Redirect } from 'react-router-dom';

import TransactionsMain from "./TransactionsMain";
import GasMain from "./gas/GasMain";

// This switch handles the transactions pages
class TransactionsRouter extends Component {
	constructor(props) {
		super(props);
	}
	
	// Might add more later, might not
	render() {
		// In the case that we don't believe the user is authenticated
		// Probably has some holes like the cache being deleted, but hopefully handled by the login page
		if (this.props.isAuthenticated === false || localStorage.getItem("status") === null) {
			return <Redirect to='/login' />
		}

		return (
			<div>
				<Switch>
					<Route exact path='/transactions' render={(props) => <TransactionsMain {...props}/>} />
					<Route exact path='/transactions/gas' render={(props) => <GasMain {...props}/>} />
					<Route render={() => (<div> Sorry, this page does not exist. </div>)} />
				</Switch>
			</div>
		);
	}
}
export default hot(module)(TransactionsRouter);